-- Adding records to the users table:

-- Currentlt, users table do not have email column so I have to alter the table and add email after username:
ALTER TABLE users
ADD COLUMN email VARCHAR(255) AFTER username;


-- [ CREATE USER DATA]
INSERT INTO users (username, email, password, datetime_created)
VALUES ('johnsmith', 'johnsmith@gmail.com', 'passwordA', '2021-01-01 01:00:00');

INSERT INTO users (username, email, password, datetime_created)
VALUES ('juandelacruz', 'juandelacruz@gmail.com', 'passwordB', '2021-01-01 02:00:00');

INSERT INTO users (username, email, password, datetime_created)
VALUES ('janesmith', 'janesmith@gmail.com', 'passwordC', '2021-01-01 03:00:00');

INSERT INTO users (username, email, password, datetime_created)
VALUES ('mariadelacruz', 'mariadelacruz@gmail.com', 'passwordD', '2021-01-01 04:00:00');

INSERT INTO users (username, email, password, datetime_created)
VALUES ('johndoe', 'johndoe@gmail.com', 'passwordE', '2021-01-01 05:00:00');

-- Running this will retrieve all records:
SELECT * FROM users;

-- Running this will retrieve all records except username:
SELECT email, password, datetime_created FROM users;

-- [CREATE A POST]
-- Author ID is the user ID.

INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES (1, 'First Code', 'Hello World!', '2021-01-02 01:00:00');

INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES (1, 'Second Code', 'Hello Earth!', '2021-01-02 02:00:00');

INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES (2, 'Third Code', 'Welcome to Mars!', '2021-01-02 03:00:00');

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES
VALUES (4, 'Fourth Code', 'Bye Bye Solar System!', '2021-01-02 04:00:00');



-- Get all the post with an Author ID of 1.
SELECT * FROM posts WHERE author_id = 1;

-- Get all the user's email and datetime of creation.
SELECT email, datetime_created FROM users;


-- Update a post's content to "Hello to the people of the Earth!" where its initial content is "Hello Earth!" by using the record's ID, which is ID 2. 
UPDATE posts
SET content = 'Hello to the people of the Earth!'
WHERE id = 2;

-- Delete user with email "johndoe@gmail.com"
-- Went from 5 users to 4.
DELETE FROM users
WHERE email = 'johndoe@gmail.com';
