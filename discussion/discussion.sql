-- [ SECTION ] INSERT records(CREATE);

-- INSERT INTO name_of_table (columns_on_table) VALUES (values_per_column);

INSERT INTO artists (name) VALUES ('Rivermaya');
INSERT INTO artists (name) VALUES ('Psy');


INSERT INTO albums(album_title, date_released, artist_id) VALUES ('Psy 6', "2012-1-1", 2);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ('Trip', "1996-1-1", 1); 

INSERT INTO songs (song_name, duration, genre, album_id) VALUES ('Gangnam Style', 253, 'K-pop', 1);
INSERT INTO songs (song_name, duration, genre, album_id) VALUES ('Kundiman', 243, 'OPM', 2);
INSERT INTO songs (song_name, duration, genre, album_id) VALUES ('Kisapmata', 259, 'OPM', 2);

--  [ SECTION ] SELECTING Records (Retrieve):
--  Retrieve all the records in a specific table:
	-- SELECT * FROM table_name;

SELECT * FROM songs;

-- Retrieve all the records but not all the columns;
SELECT song_name, duration FROM songs;

-- Retrieve record given a specific condition:
SELECT * FROM songs WHERE genre = "OPM";

SELECT song_name, duration FROM songs WHERE genre = "OPM";

-- AND and OR key word
SELECT * FROM songs WHERE duration > 240 AND genre = "OPM";
SELECT * FROM songs WHERE duration > 240 OR genre = "OPM";


-- [ SECTIOM ] UPDATING RECORDS
-- UPDATE table_name SET column_name = value WHERE condition;

UPDATE songs SET duration = 240;
UPDATE songs SET duration = 259 WHERE id = 2;


-- [ SECTION ] DELETING RECORDS
-- DELETE FROM table_name WHERE condition;
DELETE FROM songs WHERE genre = "OPM" AND length > 240;































